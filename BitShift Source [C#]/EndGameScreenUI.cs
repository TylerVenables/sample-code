using UnityEngine;
using System.Collections;

namespace BitShift
{
	public class EndGameScreenUI : MonoBehaviour
	{
		#region Variables

		public int numScoreDigits = 8;

		public Vector2 submitButtonPosPercent = new Vector2(0.5f, 0.5f);
		public Vector2 submitButtonDimensions = new Vector2(150.0f, 80.0f);
		public float submitButtonTextScale = 1.0f;

		public Vector2 backButtonPosPercent = new Vector2(0.5f, 0.5f);
		public Vector2 backButtonDimensions = new Vector2(150.0f, 80.0f);
		public float backButtonTextScale = 1.0f;

		protected GUIText messageText;
		protected GUIText scoreText;
		protected GUITexture windowBackground;
		protected GUITexture backgroundOverlay;

		protected bool hasHighscore = false;
		protected int score = 0;

		protected int messageTextFontSize = 0;
		protected int scoreTextFontSize = 0;
		protected Vector2 windowSize = Vector2.zero;

		public event BackPressedHandler BackPressedEvent;
		public delegate void BackPressedHandler();

		#endregion
		
		#region Properties

		public bool HasHighscore { get { return hasHighscore; } set { hasHighscore = value; } }

		#endregion
		
		#region Initialization
		
		public virtual void Awake ()
		{
			messageText = transform.Find("MessageText").GetComponent<GUIText>();
			scoreText = transform.Find("ScoreText").GetComponent<GUIText>();
			windowBackground = transform.Find("Window").GetComponent<GUITexture>();
			backgroundOverlay = transform.Find("BackgroundOverlay").GetComponent<GUITexture>();
		}
		
		public virtual void Start ()
		{
			UIHelper.Instance.RepositionChildUIElements(this.transform);

			backgroundOverlay.pixelInset = new Rect(Screen.width * -0.5f, Screen.height * -0.5f, Screen.width, Screen.height);
		}

		public virtual void OnEnable()
		{
			if (hasHighscore)
				messageText.text = "NEW HIGHSCORE!";
			else
				messageText.text = "FINAL SCORE";
		}
		
		#endregion
		
		#region Game Loop

		public virtual void Update ()
		{

		}

		public virtual void OnGUI()
		{
			if (score != 0)
			{
				if(UIHelper.Instance.Button("SUBMIT", submitButtonPosPercent, submitButtonDimensions, submitButtonTextScale, false, true))
					OnSubmitButtonPressed();
			}

			if(UIHelper.Instance.Button("BACK", backButtonPosPercent, backButtonDimensions, backButtonTextScale, false, true))
				OnBackButtonPressed();
		}

		#endregion
		
		#region Public Methods

		public void SetScore(int score)
		{
			this.score = score;

			string scoreString = score.ToString();
			
			scoreText.text = scoreString;
		}

		#endregion
		
		#region Private Methods

		protected void OnSubmitButtonPressed()
		{
			SocialManager.Instance.SubmitScore(score);
		}

		protected void OnBackButtonPressed()
		{
			gameObject.SetActive(false);

			BackPressedEvent();
		}

		#endregion
	}
}


