using UnityEngine;
using System.Collections;

namespace BitShift
{
	public class MainMenu : MonoBehaviour
	{
		#region Variables

		protected SceneManager sceneManager;
		protected AdHandler adHandler;

		public Texture2D buttonAsset;
		public Texture2D buttonDownAsset;
		public Font buttonFont;

		public GUISkin buttonSkin = null;

		public Vector2 playButtonPosPercent;
		public Vector2 playButtonDimensions = new Vector2(150.0f, 80.0f);
		public float playButtonTextScale = 1.0f;

		public Vector2 scoresButtonPosPercent;
		public Vector2 scoresButtonDimensions = new Vector2(150.0f, 80.0f);
		public float scoresButtonTextScale = 1.0f;

		public Vector2 achievementsButtonPosPercent;
		public Vector2 achievementsButtonDimensions = new Vector2(150.0f, 80.0f);
		public float achievementsButtonTextScale = 1.0f;

		public Vector2 rateButtonPosPercent;
		public Vector2 rateButtonDimensions = new Vector2(150.0f, 80.0f);
		public float rateButtonTextScale = 1.0f;

		private const string playButtonName = "PlayButton";
		private const string scoresButtonName = "ScoresButton";
		private const string rateButtonName = "RateButton";

		#endregion
		
		#region Properties
		
		#endregion
		
		#region Initialization
		
		public virtual void Awake ()
		{
			sceneManager = GameObject.Find("SceneManager").GetComponent<SceneManager>();
			adHandler = transform.Find("AdHandler").GetComponent<AdHandler>();
		}

		public virtual void Start ()
		{
			InitMobileSettings();

			SocialManager.Instance.Authenticate();
		}

		public void OnEnable()
		{
			adHandler.ShowAd();
		}

		public void OnDisable()
		{
			adHandler.HideAd();
		}

		#endregion
		
		#region Game Loop

		public virtual void Update ()
		{

		}

		public virtual void OnGUI()
		{
			if(UIHelper.Instance.Button("PLAY", playButtonPosPercent, playButtonDimensions * UIHelper.Instance.UIScale, playButtonTextScale * UIHelper.Instance.UIScale))
				OnPlayButtonPressed();

			if(UIHelper.Instance.Button("HIGHSCORES", scoresButtonPosPercent, scoresButtonDimensions * UIHelper.Instance.UIScale, scoresButtonTextScale * UIHelper.Instance.UIScale))
				OnScoresButtonPressed();

			if(UIHelper.Instance.Button("ACHIEVEMENTS", achievementsButtonPosPercent, achievementsButtonDimensions * UIHelper.Instance.UIScale, achievementsButtonTextScale * UIHelper.Instance.UIScale))
				OnAchievementsButtonPressed();

			if(UIHelper.Instance.Button("RATE", rateButtonPosPercent, rateButtonDimensions * UIHelper.Instance.UIScale, rateButtonTextScale * UIHelper.Instance.UIScale))
				OnRateButtonPressed();

			if (Debug.isDebugBuild)
			{
				GUIStyle style = new GUIStyle(GUI.skin.label);
				style.normal.textColor = new Color(0.6f, 0.6f, 0.6f, 1.0f);
				style.fontSize = 14;

				GUILayout.Label("Debug Keys", style);
				GUILayout.Label("Q - Show end game screen", style);
				GUILayout.Label("R - Reset highscore", style);
				GUILayout.Label("W - Reset tutorial", style);
				GUILayout.Label("S - Save level", style);
				GUILayout.Label("L - Load Level", style);
				GUILayout.Label("F - Reset level", style);
			}
		}

		/*
			if (Input.GetKeyDown(KeyCode.Q))
			{
				ui.ShowEndGameScreen(true);
			}

			if (Input.GetKeyDown(KeyCode.R))
			{
				ResetHighScore();
			}

			if (Input.GetKeyDown(KeyCode.W))
			{
				ResetTutorial();
			}

			if (Input.GetKeyDown(KeyCode.S))
			{
				SaveLevel();
			}

			if (Input.GetKeyDown(KeyCode.L))
			{
				LoadLevel();
			}

			if (Input.GetKeyDown(KeyCode.F))
			{
				ClearSavedLevel();
				ResetGame();
				StartGame();
			}
		*/

		#endregion
		
		#region Public Methods
		
		#endregion
		
		#region Private Methods

		protected void OnPlayButtonPressed()
		{
			sceneManager.ChangeScene(SceneLocation.Game);
		}

		protected void OnScoresButtonPressed()
		{
			SocialManager.Instance.ShowLeaderboard();
		}

		protected void OnAchievementsButtonPressed()
		{
			SocialManager.Instance.ShowAchievements();
		}

		protected void OnRateButtonPressed()
		{
			#if UNITY_IPHONE
				//Application.OpenURL ("market://details?id=com.example.android");
			#endif

			#if UNITY_ANDROID
				Application.OpenURL ("market://details?id=com.BitShift.BitShift");
			#endif
		}

		protected void InitMobileSettings()
		{
			Screen.autorotateToPortrait = false;
			Screen.autorotateToPortraitUpsideDown = false;
		}

		#endregion
	}
}


