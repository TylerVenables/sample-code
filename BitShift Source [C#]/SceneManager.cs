using UnityEngine;
using System.Collections;

namespace BitShift
{
	public enum SceneLocation { MainMenu, Game }

	public class SceneManager : MonoBehaviour
	{
		#region Variables

		GameObject mainMenu;
		GameObject game;

		SceneLocation currentScene = SceneLocation.MainMenu;

		#endregion
		
		#region Properties

		public SceneLocation CurrentScene { get { return currentScene; } }

		#endregion
		
		#region Initialization
		
		public virtual void Awake ()
		{

		}

		public virtual void Start ()
		{
			mainMenu = GameObject.Find("MainMenu");
			game = GameObject.Find("Game");

			mainMenu.SetActive(true);
			game.SetActive(false);
		}
		
		#endregion
		
		#region Game Loop

		public virtual void Update ()
		{

		}
		
		#endregion
		
		#region Public Methods

		public bool ChangeScene(SceneLocation newScene)
		{
			if (currentScene == newScene)
			{
				return false;
			}

			switch(currentScene)
			{
			case SceneLocation.MainMenu:
				mainMenu.SetActive(false);
				break;
			case SceneLocation.Game:
				game.SetActive(false);
				break;
			}

			currentScene = newScene;

			switch(newScene)
			{
			case SceneLocation.MainMenu:
				mainMenu.SetActive(true);
				break;
			case SceneLocation.Game:
				game.SetActive(true);
				break;
			}

			return true;
		}

		#endregion
		
		#region Private Methods
		
		#endregion
	}
}


