using UnityEngine;
using System.Collections;

namespace BitShift
{
	public class TouchInput : MonoBehaviour
	{
		#region Variables

		public float vertSwipeDist = 120.0f;
		public float minHorSwipeDist  = 50.0f;
		public float maxSwipeTime = 0.5f;

		private float fingerStartTime  = 0.0f;
		private Vector2 fingerStartPos = Vector2.zero;
		private Vector2 prevTouchPos = Vector2.zero;

		private float swipeDist = 0.0f;
		
		private bool isSwipe = false;

		private bool swipeUp = false;
		private bool swipeDown = false;
		private bool swipeLeft = false;
		private bool swipeRight = false;

		private bool hasMovedTile = false;

		private static TouchInput instance;

		#endregion
		
		#region Properties
		
		public bool SwipeUp { get { return swipeUp; } }
		public bool SwipeDown { get { return swipeDown; } }
		public bool SwipeLeft { get { return swipeLeft; } }
		public bool SwipeRight { get { return swipeRight; } }

		public static TouchInput Instance
		{
			get
			{
				if (instance == null)
					instance = GameObject.FindObjectOfType<TouchInput>();

				return instance;
			}
		}

		#endregion
		
		#region Initialization
		
		public virtual void Awake ()
		{
		
		}
		
		public virtual void Start ()
		{
		
		}

		#endregion
		
		#region Game Loop

		public virtual void Update ()
		{
			swipeUp = false;
			swipeDown = false;
			swipeLeft = false;
			swipeRight = false;

			if (Input.touchCount > 0){
				
				foreach (Touch touch in Input.touches)
				{
					switch (touch.phase)
					{
					case TouchPhase.Began :
						// this is a new touch
						isSwipe = true;
						fingerStartTime = Time.time;
						fingerStartPos = touch.position;
						prevTouchPos = touch.position;
						hasMovedTile = false;
						break;
						
					case TouchPhase.Canceled :
						// The touch is being canceled
						isSwipe = false;
						hasMovedTile = false;
						break;
						
					case TouchPhase.Ended:
						
						float gestureTime = Time.time - fingerStartTime;
						float gestureDist = (touch.position - fingerStartPos).magnitude;
						
						if (isSwipe && gestureTime < maxSwipeTime && gestureDist > minHorSwipeDist){
							Vector2 direction = touch.position - fingerStartPos;
							Vector2 swipeType = Vector2.zero;
							
							if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y)){
								// the swipe is horizontal:
								swipeType = Vector2.right * Mathf.Sign(direction.x);
							}
							
							if(swipeType.x != 0.0f && !hasMovedTile){
								if(swipeType.x > 0.0f){
									swipeRight = true;
								}else{
									swipeLeft = true;
								}
							}
						}

						break;

					case TouchPhase.Moved:
						Vector2 diff = touch.position - prevTouchPos;

						if (Mathf.Abs(diff.normalized.y) >= 0.7f)
						{
							if ((swipeDist < 0.0f && diff.y > 0.0f) || (swipeDist > 0.0f && diff.y < 0.0f))
								swipeDist = diff.y;
							else
							{
								swipeDist += diff.y;
							}
						}

						if(swipeDist > vertSwipeDist || swipeDist < -vertSwipeDist)
						{
							if (swipeDist > 0.0f)
								swipeUp = true;
							else
								swipeDown = true;

							swipeDist = 0.0f;

							hasMovedTile = true;
						}

						prevTouchPos = touch.position;
						break;
					}
				}
			}
		}

		#endregion
		
		#region Public Methods
		
		#endregion
		
		#region Private Methods
		
		#endregion
	}
}


