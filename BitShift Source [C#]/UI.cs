using UnityEngine;
using System.Collections;

namespace BitShift
{
	public class UI : MonoBehaviour
	{
		#region Variables

		protected GUIText levelText;
		protected GUIText scoreText;
		protected GUIText highscoreLabelText;
		protected GUIText highscoreText;

		protected EndGameScreenUI endGameScreen;
		protected TutorialScreen tutorialScreen;

		public int numScoreDigits = 10;
		public int scoreIncreaseSpeed = 200;

		public Vector2 pauseButtonPosPercent = new Vector2(0.5f, 0.5f);
		public Vector2 pauseButtonDimensions = new Vector2(150.0f, 80.0f);
		public float pauseButtonTextScale = 1.0f;

		protected int targetScore = 0;
		protected int showingScore = 0;
		protected int highScore = 0;
		protected int level = 1;

		protected bool isPaused = false;
		protected bool isWaitingForEndGameScreen = false;
		protected bool isShowingEndGameScreen = false;

		public float endGameScreenDelay = 1.0f;
		protected float endGameScreenTimer = 0.0f;

		protected const float labelWidth = 200.0f;

		protected int levelTextFontSize = 0;
		protected int scoreTextFontSize = 0;
		protected int highscoreLabelTextFontSize = 0;
		protected int highscoreTextFontSize = 0;

		protected bool hidePauseButton = false;

		public event PausePressedHandler PausePressedEvent;
		public delegate void PausePressedHandler();

		public event BackPressedHandler BackPressedEvent;
		public delegate void BackPressedHandler();

		public event TutorialDoneHandler TutorialDoneEvent;
		public delegate void TutorialDoneHandler();

		#endregion
		
		#region Properties
		
		#endregion
		
		#region Initialization
		
		public virtual void Awake ()
		{

		}

		public virtual void Start ()
		{
			levelText = transform.Find("LevelText").GetComponent<GUIText>();
			scoreText = transform.Find("ScoreText").GetComponent<GUIText>();
			highscoreLabelText = transform.Find("HighscoreLabelText").GetComponent<GUIText>();
			highscoreText = transform.Find("HighscoreText").GetComponent<GUIText>();

			endGameScreen = transform.Find("EndGameScreen").GetComponent<EndGameScreenUI>();
			endGameScreen.gameObject.SetActive(false);
			endGameScreen.BackPressedEvent += OnBackPressed;

			tutorialScreen = transform.Find("TutorialScreen").GetComponent<TutorialScreen>();
			tutorialScreen.gameObject.SetActive(false);
			tutorialScreen.TutorialDoneEvent += OnTutorialComplete;

			levelTextFontSize = levelText.fontSize;
			scoreTextFontSize = scoreText.fontSize;
			highscoreLabelTextFontSize = highscoreLabelText.fontSize;
			highscoreTextFontSize = highscoreText.fontSize;

			levelText.fontSize = (int)((float)levelTextFontSize * UIHelper.Instance.UIScale);
			scoreText.fontSize = (int)((float)scoreTextFontSize * UIHelper.Instance.UIScale);
			highscoreLabelText.fontSize = (int)((float)highscoreLabelTextFontSize * UIHelper.Instance.UIScale);
			highscoreText.fontSize = (int)((float)highscoreTextFontSize * UIHelper.Instance.UIScale);
		}

		#endregion
		
		#region Game Loop

		public virtual void Update ()
		{
			if (!isPaused)
			{
				if (showingScore < targetScore)
				{
					showingScore += Mathf.FloorToInt(scoreIncreaseSpeed * Time.deltaTime);

					if (showingScore > targetScore)
						showingScore = targetScore;
				}

				string scoreString = showingScore.ToString();
				int numDigits = scoreString.Length;

				for (int i = 0; i < numScoreDigits - numDigits; i++)
					scoreString = "0" + scoreString;

				scoreText.text = scoreString;
			}

			if (isWaitingForEndGameScreen && endGameScreenTimer < endGameScreenDelay)
			{
				endGameScreenTimer += Time.deltaTime;

				if (endGameScreenTimer > endGameScreenDelay)
				{
					isWaitingForEndGameScreen = false;
					isShowingEndGameScreen = true;
					hidePauseButton = true;

					endGameScreen.SetScore(targetScore);

					endGameScreen.gameObject.SetActive(true);
				}
			}

			if (Debug.isDebugBuild) 
			{
				levelText.fontSize = (int)((float)levelTextFontSize * UIHelper.Instance.UIScale);
				scoreText.fontSize = (int)((float)scoreTextFontSize * UIHelper.Instance.UIScale);
				highscoreLabelText.fontSize = (int)((float)highscoreLabelTextFontSize * UIHelper.Instance.UIScale);
	            highscoreText.fontSize = (int)((float)highscoreTextFontSize * UIHelper.Instance.UIScale);
			}
		}

		public void OnGUI()
		{
			if (!hidePauseButton)
			{
				if (UIHelper.Instance.Button("PAUSE", pauseButtonPosPercent, pauseButtonDimensions, pauseButtonTextScale))
				    OnPausePressed();
			}
		}

		#endregion
		
		#region Public Methods

		public void UpdateScore(int score, bool showAnimation = true)
		{
			targetScore = score;

			if (!showAnimation || score < showingScore)
			{
				showingScore = score;
			}
		}

		public void UpdateLevel(int level)
		{
			this.level = level;

			string levelString = "Level " + level.ToString();

			levelText.text = levelString;
		}

		public void UpdateHighScore(int score)
		{
			highScore = score;

			string scoreString = highScore.ToString();
			int numDigits = scoreString.Length;
			
			for (int i = 0; i < numScoreDigits - numDigits; i++)
				scoreString = "0" + scoreString;

			highscoreText.text = scoreString;
			endGameScreen.SetScore(score);
		}

		public void Pause()
		{
			isPaused = true;
		}
		
		public void UnPause()
		{
			isPaused = false;

			isShowingEndGameScreen = false;
			hidePauseButton = false;
			isWaitingForEndGameScreen = false;
		}

		public void ShowEndGameScreen(bool hasHighscore = false)
		{
			endGameScreenTimer = 0.0f;
			isWaitingForEndGameScreen = true;

			endGameScreen.HasHighscore = hasHighscore;
		}

		public void ShowTutorialScreen()
		{
			tutorialScreen.gameObject.SetActive(true);

			hidePauseButton = true;
		}

		#endregion
		
		#region Private Methods

		protected void OnPausePressed()
		{
			if (isShowingEndGameScreen || isWaitingForEndGameScreen)
				return;

			PausePressedEvent();
		}

		protected void OnTutorialComplete()
		{
			tutorialScreen.gameObject.SetActive(false);

			hidePauseButton = false;

			TutorialDoneEvent();
		}

		protected void OnBackPressed()
		{
			BackPressedEvent();
		}

		#endregion
	}
}


