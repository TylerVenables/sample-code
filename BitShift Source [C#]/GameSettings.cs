using UnityEngine;
using System.Collections;

namespace BitShift
{
	public class GameSettings : MonoBehaviour
	{
		public int numTilesForMatch = 4;

		public float tileStartDistance = 7.0f;

		public int scorePerMatch = 150;
		public float scoreMultiplierPerTile = 1.2f;
		public float scoreMultiplierPerMatch = 1.2f;
		
		public int scorePerLevel = 1000;

		public float tileStartSpeed = 1.0f;
		public float tileSpeedPerLevel = 0.01f;
		public float maxTileSpeed = 2.0f;
		public float tileShiftSpeed = 2.5f;
		public float tileSpawnTime = 1.0f;

		public int startColors = 3;
		public int maxColors = 5;
		public int levelsPerNewColor = 5;
		public int startLevelForNewColor = 3;

		public int bombStartLevel = 5;
		public float bombChance = 0.05f;
		public int minTilesBetweenBombs = 5;

		public int blockerStartLevel = 12;
		public float blockerChance = 0.05f;
		public int minTilesBetweenBlockers = 3;
		public int maxBlockers = 5;

		public float timeBetweenRounds = 1.5f;

		public Color[] tileColors;

		public virtual void Awake ()
		{
			
		}
		
		public virtual void Start ()
		{

		}
	}
}


