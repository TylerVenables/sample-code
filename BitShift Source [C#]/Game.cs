using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace BitShift
{
	public class Game : MonoBehaviour
	{
		#region Variables

		public GameObject tileAsset;
		public GameObject warningIndicatorAsset;

		protected SceneManager sceneManager;
		protected Grid grid;
		protected UI ui;
		protected GameSettings gameSettings;

		protected Tile currentTile;
		List<Tile> tileList;
		
		List<WarningIndicator> warningIndicatorList;

		protected float tileMoveSpeed = 1.0f;
		protected int numTileColors = 3;

		protected float spawnTimer = 0.0f;

		protected bool isWaiting = false;
		protected bool isPaused = false;

		protected bool isBetweenRounds = false;
		protected float roundTimer = 0.0f;

		protected MoveDirection lastTileDirection;

		protected int tilesSinceBomb = 0;
		protected int tilesSinceBlocker = 0;
		protected int numBlockers = 0;

		protected int numTilesWaiting = 0;

		protected int score = 0;
		protected int level = 1;

		protected int highScore = 0;

		protected bool showTutorial = false;

		protected TileColor savedColor = TileColor.Blue;
		protected TileType savedType = TileType.Regular;
		protected MoveDirection savedDirection = MoveDirection.Right;
		protected bool usedSavedTile = false;

		#endregion
		
		#region Properties
		
		#endregion
		
		#region Initialization
		
		public virtual void Awake ()
		{

		}

		public virtual void Start ()
		{
			sceneManager = GameObject.Find("SceneManager").GetComponent<SceneManager>();
			grid = transform.Find("Grid").GetComponent<Grid>();
			ui = transform.Find("UI").GetComponent<UI>();
			gameSettings = transform.Find("GameSettings").GetComponent<GameSettings>();

			ui.PausePressedEvent += OnPausePressed;
			ui.TutorialDoneEvent += OnTutorialDone;
			ui.BackPressedEvent += OnBackToMainMenuPressed;

			currentTile = null;

			tileList = new List<Tile>();
			warningIndicatorList = new List<WarningIndicator>();

			spawnTimer = 0.0f;
			lastTileDirection = MoveDirection.Left;

			tileMoveSpeed = gameSettings.tileStartSpeed;
			numTileColors = gameSettings.startColors;

			LoadHighScore();
			ui.UpdateHighScore(highScore);

			LoadTutorial();

			if (showTutorial)
			{
				Pause();
				ui.ShowTutorialScreen();
			}
			else
			{
				LoadLevel();
				StartGame();
			}
		}

		public virtual void OnEnable()
		{
			if (sceneManager != null && sceneManager.CurrentScene == SceneLocation.Game)
			{
				if (!showTutorial)
				{
					UnPause();
					StartGame();
				}
			}
		}

		public virtual void OnApplicationPause(bool pause)
		{
			if (pause && !isBetweenRounds)
			{
				Pause();
				sceneManager.ChangeScene(SceneLocation.MainMenu);
			}
		}

		public virtual void OnApplicationQuit()
		{
			SaveLevel();
		}

		#endregion
		
		#region Game Loop

		public virtual void Update ()
		{
			if (!isPaused && !isBetweenRounds)
			{
				HandleInput();

				if (!isWaiting)
				{
					spawnTimer += Time.deltaTime;

					if (spawnTimer >= gameSettings.tileSpawnTime)
					{
						CreateGameTile();
						spawnTimer = 0.0f;
						isWaiting = true;
					}
				}
			}
		}
		
		#endregion
		
		#region Public Methods

		public void CreateGameTile()
		{
			GameObject tileObject = SpawnTile();
			Tile tile = tileObject.GetComponent<Tile>();
			float startX = gameSettings.tileStartDistance * grid.GridScale;

			bool isRegularTile = true;

			if (!usedSavedTile)
			{
				if (level >= gameSettings.bombStartLevel && tilesSinceBomb >= gameSettings.minTilesBetweenBombs)
				{
					float num = UnityEngine.Random.Range(0.0f, 1.0f);

					if (num < gameSettings.bombChance)
					{
						isRegularTile = false;
						tile.SetType(TileType.Bomb);
						tilesSinceBomb = 0;
					}
				}

				if (isRegularTile)
				{
					if (level >= gameSettings.blockerStartLevel && tilesSinceBlocker >= gameSettings.minTilesBetweenBlockers && numBlockers < gameSettings.maxBlockers)
					{
						float num = UnityEngine.Random.Range(0.0f, 1.0f);
						
						if (num < gameSettings.blockerChance)
						{
							isRegularTile = false;
							tile.SetType(TileType.Blocker);
							tilesSinceBlocker = 0;
							numBlockers++;
						}
					}
				}

				if (isRegularTile)
				{
					tile.RandomColor(numTileColors);
					tilesSinceBomb++;
					tilesSinceBlocker++;
				}
				else if (tile.Type == TileType.Bomb)
				{
					if(grid.CheckForAllTileTypes(gameSettings.maxColors))
						SocialManager.Instance.UnlockAchievement(AchievementName.Multichromatic);
				}

				if (lastTileDirection == MoveDirection.Left)
				{
					startX = -startX;
					tile.SetDirection(MoveDirection.Right);

					lastTileDirection = MoveDirection.Right;
				}
				else
				{
					lastTileDirection = MoveDirection.Left;
				}
			}
			else
			{
				tile.SetType(savedType);
				tile.SetColor(savedColor);
				tile.SetDirection(savedDirection);
				lastTileDirection = savedDirection;

				if (savedDirection == MoveDirection.Right)
					startX = -startX;

				usedSavedTile = false;
			}


			tile.MoveSpeed = tileMoveSpeed * grid.GridScale;
			tile.ShiftSpeed = gameSettings.tileShiftSpeed * grid.GridScale;

			int row = UnityEngine.Random.Range(0, grid.rows);
			float z = -1.0f;

			tileObject.transform.position = new Vector3(startX, tileObject.transform.position.z, z);
			tile.SetRow(row);

			currentTile = tile;
			currentTile.StateChangedEvent += OnTileStateChanged;
		}

		public GameObject SpawnTile()
		{
			GameObject newTile = (GameObject)GameObject.Instantiate(tileAsset, Vector3.zero, Quaternion.identity);

			newTile.transform.parent = transform;

			tileList.Add(newTile.GetComponent<Tile>());

			return newTile;
		}

		public void ShiftRow(int row, MoveDirection direction)
		{
			Tile tile = null;
			int startColumn = direction == MoveDirection.Left ? 0 : grid.columns - 1;
			int currentColumn = startColumn;

			numTilesWaiting = 0;

			for (int i = 0; i < grid.columns; i++)
			{
				tile = grid.CheckGrid(currentColumn, row);

				if (tile != null)
					ShiftTile(tile, direction);

				if (direction == MoveDirection.Left)
					currentColumn++;
				else
					currentColumn--;
			}

			if (currentTile != null && currentTile.CurrentColumn == -1)
				ShiftTile(currentTile, direction);
		}

		protected void ShiftTile(Tile tile, MoveDirection direction)
		{
			int newColumn = 0;

			if (tile.CurrentColumn != -1)
				newColumn = direction == MoveDirection.Left ? tile.CurrentColumn - 1 : tile.CurrentColumn + 1;
			else
				newColumn = direction == MoveDirection.Left ? grid.columns - 1 : 0;

			if (newColumn >= 0 && newColumn < grid.columns)
			{
				tile.Move(newColumn, tile.CurrentRow);
			}
			else
			{
				tile.RemoveFromGrid();
			}

			tile.SetState(TileState.Shifting);
			tile.SetDirection(direction);
			
			numTilesWaiting++;
		}
		
		public void RemoveTile(Tile tile)
		{
			grid.RemoveTile(tile);

			tile.OnRemove();

			tileList.Remove(tile);

			Destroy(tile.gameObject);
		}

		public void OnLoss(bool startLeft)
		{
			isWaiting = true;
			isBetweenRounds = true;

			Tile tile = null;

			for (int i = 0; i < tileList.Count; i++)
			{
				tile = tileList[i];

				tile.StartLoseAnimation(startLeft);

				numTilesWaiting++;
			}

			if (score > highScore)
			{
				highScore = score;
				SaveHighScore(highScore);

				ui.UpdateHighScore(highScore);

				ui.ShowEndGameScreen(true);
			}
			else
			{
				ui.ShowEndGameScreen(false);
			}

			ClearSavedLevel();
		}

		public void OnPausePressed()
		{
			if (!isPaused)
			{
				Pause();

				sceneManager.ChangeScene(SceneLocation.MainMenu);
			}
			else
			{
				UnPause();
			}
		}

		public void Pause()
		{
			SaveLevel();

			if (isPaused)
				return;

			isPaused = true;

			for (int i = 0; i < tileList.Count; i++)
			{
				tileList[i].Pause();
			}

			ui.Pause();
		}

		public void UnPause()
		{
			if (!isPaused)
				return;

			isPaused = false;
			
			for (int i = 0; i < tileList.Count; i++)
			{
				tileList[i].UnPause();
			}

			ui.UnPause();
		}

		public void OnBackToMainMenuPressed()
		{
			sceneManager.ChangeScene(SceneLocation.MainMenu);

			ResetGame();

			Pause();
		}

		#endregion
		
		#region Private Methods

		protected void HandleInput()
		{
			bool moveUp = (Input.GetKeyDown("up") || TouchInput.Instance.SwipeUp);
			bool moveDown = (Input.GetKeyDown("down") || TouchInput.Instance.SwipeDown);
			bool moveLeft = (Input.GetKeyDown("left") || TouchInput.Instance.SwipeLeft);
			bool moveRight = (Input.GetKeyDown("right") || TouchInput.Instance.SwipeRight);

			if (currentTile != null && currentTile.State == TileState.Moving)
			{
				if (moveUp || moveDown)
				{
					int newRow = currentTile.CurrentRow + 1;

					if (moveDown)
						newRow = currentTile.CurrentRow - 1;

					currentTile.Move(-1, newRow, true);
				}
				else if ((moveLeft && currentTile.MoveDirection == MoveDirection.Left) || 
				         (moveRight && currentTile.MoveDirection == MoveDirection.Right))
				{
					int col = grid.FindEndOfRow(currentTile.CurrentRow, currentTile.MoveDirection);

					if (col != -1)
					{
						currentTile.Move(col, currentTile.CurrentRow, true);
					}
					else
					{
						float pos = grid.AdjustedTileSize * grid.HalfColumns + grid.HalfTileSize;

						if (currentTile.MoveDirection == MoveDirection.Right)
							pos = -pos;

						currentTile.transform.position = new Vector3(pos, currentTile.transform.position.y, currentTile.transform.position.z);
					}

					currentTile.SetState(TileState.Idle);
				}
			}

			if (Debug.isDebugBuild) 
			{
				if (Input.GetKeyDown(KeyCode.Q))
				{
					ui.ShowEndGameScreen(true);
				}

				if (Input.GetKeyDown(KeyCode.R))
				{
					ResetHighScore();
				}

				if (Input.GetKeyDown(KeyCode.W))
				{
					ResetTutorial();
				}

				if (Input.GetKeyDown(KeyCode.S))
				{
					SaveLevel();
				}

				if (Input.GetKeyDown(KeyCode.L))
				{
					LoadLevel();
				}

				if (Input.GetKeyDown(KeyCode.F))
				{
					ClearSavedLevel();
					ResetGame();
					StartGame();
				}
			}
		}

		protected void OnTileStateChanged(Tile sender, TileState newState, TileState previousState)
		{
			if (newState == TileState.Idle) 
			{
				if (previousState == TileState.Shifting || previousState == TileState.Collapsing || previousState == TileState.LossAnim2)
				{
					numTilesWaiting--;

					if (numTilesWaiting == 0)
					{
						if (previousState == TileState.Shifting)
						{
							if (!CheckForLoss())
								CheckForMatches();
						}
						else if (previousState == TileState.Collapsing)
						{
							CheckForMatches();
						}
						else if (previousState == TileState.LossAnim2)
						{
							OnLossAnimComplete();
						}
					}
				}
				else if (previousState == TileState.Moving)
				{
					if (currentTile != null && sender == currentTile)
					{
						if (currentTile.Type == TileType.Bomb)
						{
							if (currentTile.IsInMidColumn())
							{
								isWaiting = false;
							}
							else
							{
								int colToCheck = 0;
								
								if (currentTile.MoveDirection == MoveDirection.Left)
								{
									if (currentTile.CurrentColumn == -1)
										colToCheck = grid.columns - 1;
									else
										colToCheck = currentTile.CurrentColumn - 1;
								}
								else
								{
									if (currentTile.CurrentColumn != -1)
										colToCheck = currentTile.CurrentColumn + 1;
								}

								Tile tileToUse = grid.CheckGrid(colToCheck, currentTile.CurrentRow);

								UseBomb(tileToUse.TileColor, tileToUse.Type);
							}
							
							RemoveTile(currentTile);
						}
						else
						{
							if (!currentTile.IsInMidColumn())
							{
								ShiftRow(currentTile.CurrentRow, currentTile.MoveDirection);
							}
							else
							{
								CheckForMatches();
							}
						}
						
						currentTile = null;
					}
				}
			}
		}

		protected bool CheckForMatches()
		{
			List<Tile> matchingTiles;

			bool hasMatches = grid.CheckForMatches(gameSettings.numTilesForMatch, out matchingTiles);

			if (hasMatches)
			{
				Tile tile = null;
				int tileCount = 0;
				TileColor color = TileColor.Blue;
				int column = 0;
				bool doneGroup = false;
				int numMatches = 0;
				float matchScore = 0;
				float scoreToAdd = 0;

				for (int i = 0; i < matchingTiles.Count; i++)
				{
					tile = matchingTiles[i];

					if (i == 0)
					{
						color = tile.TileColor;
						column = tile.CurrentColumn;
						tileCount = 1;
					}
					else if (tile.TileColor != color || tile.CurrentColumn != column || i == matchingTiles.Count - 1)
					{
						doneGroup = true;

						if(tile.TileColor != color)
							SocialManager.Instance.UnlockAchievement(AchievementName.Shifty);
					}
					else
					{
						tileCount++;
					}

					if (doneGroup)
					{
						if (i == matchingTiles.Count - 1)
						{
							tileCount++;
						}

						int extraTiles = tileCount - gameSettings.numTilesForMatch;
						matchScore = gameSettings.scorePerMatch * Mathf.Pow(gameSettings.scoreMultiplierPerTile, extraTiles);

						scoreToAdd += matchScore;

						numMatches++;
						doneGroup = false;

						color = tile.TileColor;
						column = tile.CurrentColumn;

						if (tileCount >= 6)
							SocialManager.Instance.UnlockAchievement(AchievementName.MatchMaker);

						if (tileCount >= 7)
							SocialManager.Instance.UnlockAchievement(AchievementName.MatchMaster);

						tileCount = 1;
					}
				}

				scoreToAdd = scoreToAdd * Mathf.Pow(gameSettings.scoreMultiplierPerMatch, numMatches - 1);
				AddScore(Mathf.FloorToInt(scoreToAdd));

				for (int i = 0; i < matchingTiles.Count; i++)
				{
					RemoveTile(matchingTiles[i]);
				}

				matchingTiles.Clear();

				if(!CollapseTiles())
					isWaiting = false;
			}
			else
			{
				isWaiting = false;
			}

			CheckForWarning();

			return hasMatches;
		}

		protected bool CollapseTiles()
		{
			Tile tile = null;

			List<Tile> collapsedTiles;

			grid.CollapseTiles(out collapsedTiles);

			if (collapsedTiles.Count == 0)
				return false;

			for (int i = 0; i < collapsedTiles.Count; i++)
			{
				tile = collapsedTiles[i];

				if (tile.CurrentColumn >= grid.HalfColumns)
					tile.SetDirection(MoveDirection.Left);
				else
					tile.SetDirection(MoveDirection.Right);

				tile.SetState(TileState.Collapsing);

				numTilesWaiting++;
			}

			return true;
		}

		protected void CheckForWarning()
		{
			Tile tile = null;
			List<int> warningRows = new List<int>();

			for (int i = 0; i < tileList.Count; i++)
			{
				tile = tileList[i];
				
				if (tile.CurrentColumn == 0 || tile.CurrentColumn == grid.columns - 1)
				{
					warningRows.Add(tile.CurrentRow);
				}
			}

			for (int i = 0; i < grid.rows; i++)
			{
				WarningIndicator indicator = null;

				for (int j = 0; j < warningIndicatorList.Count; j++)
				{
					if (warningIndicatorList[j].Row == i)
					{
						indicator = warningIndicatorList[j];
						break;
					}
				}

				bool shouldHaveIndicator = false;

				for (int j = 0; j < warningRows.Count; j++)
				{
					if (warningRows[j] == i)
					{
						shouldHaveIndicator = true;
						break;
					}
				}

				if (indicator != null && shouldHaveIndicator == false)
				{
					Destroy(indicator.gameObject);
					warningIndicatorList.Remove(indicator);
				}
				else if (indicator == null && shouldHaveIndicator == true)
				{
					GameObject newIndicator = (GameObject)GameObject.Instantiate(warningIndicatorAsset, Vector3.zero, Quaternion.identity);
					((WarningIndicator)newIndicator.GetComponent<WarningIndicator>()).SetRow(i);

					newIndicator.transform.localScale = new Vector3(grid.GridScale, grid.GridScale, 1.0f);

					warningIndicatorList.Add(newIndicator.GetComponent<WarningIndicator>());
				}
			}
		}

		protected bool CheckForLoss()
		{
			Tile tile = null;

			for (int i = 0; i < tileList.Count; i++)
			{
				tile = tileList[i];

				if (tile.CurrentColumn == -1)
				{
					OnLoss(tile.transform.position.x < 0.0f);
					return true;
				}
			}

			return false;
		}

		protected void AddScore(int scoreToAdd)
		{
			if (scoreToAdd <= 0)
				return;

			score += scoreToAdd;
			ui.UpdateScore(score);

			UpdateLevel();
		}

		protected void UpdateLevel()
		{
			int newLevel = Mathf.FloorToInt(score / gameSettings.scorePerLevel) + 1;

			if (newLevel > level)
			{
				level = newLevel;

				tileMoveSpeed = gameSettings.tileStartSpeed + (gameSettings.tileSpeedPerLevel * level);

				if (tileMoveSpeed > gameSettings.maxTileSpeed)
					tileMoveSpeed = gameSettings.maxTileSpeed;

				int numColors = gameSettings.startColors;

				if (level >= gameSettings.startLevelForNewColor)
				{
					numColors++;

					numColors += Mathf.FloorToInt((level - gameSettings.startLevelForNewColor) / gameSettings.levelsPerNewColor);

					if (numColors >= gameSettings.maxColors)
						numColors = gameSettings.maxColors;

					numTileColors = numColors;
				}

				if (level == 10)
					SocialManager.Instance.UnlockAchievement(AchievementName.BeginnerShifter);
				else if (level == 15)
					SocialManager.Instance.UnlockAchievement(AchievementName.NoviceShifter);
				else if (level == 20)
					SocialManager.Instance.UnlockAchievement(AchievementName.ExpertShifter);

				ui.UpdateLevel(level);
			}
		}

		protected void OnLossAnimComplete()
		{
			for (int i = 0; i < warningIndicatorList.Count; i++)
				Destroy(warningIndicatorList[i].gameObject);

			warningIndicatorList.Clear();
		}

		protected void ResetGame()
		{
			grid.ClearGrid();
			
			for (int i = 0; i < tileList.Count; i++)
			{
				Destroy(tileList[i].gameObject);
			}
			
			tileList.Clear();

			spawnTimer = gameSettings.tileSpawnTime;
			lastTileDirection = MoveDirection.Left;
			
			tileMoveSpeed = gameSettings.tileStartSpeed;
			numTileColors = gameSettings.startColors;

			tilesSinceBomb = 0;
			tilesSinceBlocker = 0;
			numBlockers = 0;
			spawnTimer = 0.0f;

			currentTile = null;

			for (int i = 0; i < warningIndicatorList.Count; i++)
				Destroy(warningIndicatorList[i].gameObject);

			warningIndicatorList.Clear();

			score = 0;
			level = 1;

			ui.UpdateLevel(level);
			ui.UpdateScore(score, false);

			isWaiting = false;
		}

		protected void StartGame()
		{
			isBetweenRounds = false;
		}

		protected void UseBomb(TileColor colorToDestroy, TileType type)
		{
			List<Tile> tilesToRemove = new List<Tile>();

			for (int i = 0; i < tileList.Count; i++)
			{
				if (type == TileType.Regular)
				{
					if (tileList[i].TileColor == colorToDestroy)
						tilesToRemove.Add(tileList[i]);
				}
				else
				{
					if (tileList[i].Type == type)
						tilesToRemove.Add(tileList[i]);
				}
			}

			for (int i = 0; i < tilesToRemove.Count; i++)
			{
				RemoveTile(tilesToRemove[i]);
			}

			tilesToRemove.Clear();

			if (type == TileType.Blocker)
			{
				tilesSinceBlocker = 0;
				numBlockers = 0;
			}

			if (!CollapseTiles())
			{
				CheckForWarning();
				isWaiting = false;
			}
		}

		protected void SaveLevel()
		{
			if (!isBetweenRounds)
			{
				string gridString = grid.GetGridString();

				//Debug.Log(gridString);

				PlayerPrefs.SetString("SavedLevel", gridString);
				PlayerPrefs.SetInt("SavedScore", score);

				if (currentTile != null)
				{
					PlayerPrefs.SetInt("SavedColor", (int)currentTile.TileColor);
					PlayerPrefs.SetInt("SavedType", (int)currentTile.Type);
					PlayerPrefs.SetInt("SavedDirection", (int)currentTile.MoveDirection);
				}
				else
				{
					PlayerPrefs.DeleteKey("SavedColor");
					PlayerPrefs.DeleteKey("SavedType");

					if (lastTileDirection == MoveDirection.Left)
						PlayerPrefs.SetInt("SavedDirection", (int)MoveDirection.Right);
					else
						PlayerPrefs.SetInt("SavedDirection", (int)MoveDirection.Left);
				}

				/*
				Debug.Log("SAVE");
				Debug.Log(currentTile.TileColor);
				Debug.Log(currentTile.Type);
				Debug.Log(currentTile.MoveDirection);
				*/
			}
		}

		protected void ClearSavedLevel()
		{
			//Debug.Log("CLEAR");

			PlayerPrefs.DeleteKey("SavedLevel");
			PlayerPrefs.DeleteKey("SavedScore");
			PlayerPrefs.DeleteKey("SavedColor");
			PlayerPrefs.DeleteKey("SavedType");
			PlayerPrefs.DeleteKey("SavedDirection");
		}

		protected void LoadLevel()
		{
			string levelString = "";

			if (PlayerPrefs.HasKey("SavedLevel"))
				levelString = PlayerPrefs.GetString("SavedLevel");

			ResetGame();

			if (levelString != "")
			{
				int row = 0;
				int col = 0;
				int type = 0;

				int index = 0;

				while (index < levelString.Length - 1)
				{
					col = (int)Char.GetNumericValue(levelString[index]);

					index++;

					do
					{
						row = (int)Char.GetNumericValue(levelString[index]);
						type = (int)Char.GetNumericValue(levelString[index + 1]);

						GameObject tileObject = SpawnTile();
						Tile tile = tileObject.GetComponent<Tile>();

						tile.StateChangedEvent += OnTileStateChanged;

						if (type < (int)TileColor.None)
						{
							tile.SetType(TileType.Regular);
							tile.SetColor((TileColor)type);
						}
						else
						{
							tile.SetType(TileType.Blocker);
							tile.SetColor(TileColor.None);
						}

						tile.Move(col, row, true);
						tile.SetState(TileState.Idle);
						tile.MoveSpeed = tileMoveSpeed * grid.GridScale;
						tile.ShiftSpeed = gameSettings.tileShiftSpeed * grid.GridScale;

						tile.transform.position += new Vector3(0.0f, 0.0f, -1.0f);

						index += 2;

					} while (index < levelString.Length - 1 && levelString[index] != '|');

					index++;
				}

				CheckForMatches();

				usedSavedTile = true;

				if (PlayerPrefs.HasKey("SavedScore"))
					score = PlayerPrefs.GetInt("SavedScore");

				if (PlayerPrefs.HasKey("SavedColor"))
					savedColor = (TileColor)PlayerPrefs.GetInt("SavedColor");
				else 
					usedSavedTile = false;

				if (PlayerPrefs.HasKey("SavedType"))
					savedType = (TileType)PlayerPrefs.GetInt("SavedType");
				else 
					usedSavedTile = false;

				if (PlayerPrefs.HasKey("SavedDirection"))
					savedDirection = (MoveDirection)PlayerPrefs.GetInt("SavedDirection");
				else 
					usedSavedTile = false;

				/*
				Debug.Log("LOAD");
				Debug.Log((TileColor)PlayerPrefs.GetInt("SavedColor"));
				Debug.Log((TileType)PlayerPrefs.GetInt("SavedType"));
				Debug.Log((MoveDirection)PlayerPrefs.GetInt("SavedDirection"));
				*/

				numBlockers = grid.CountTilesOfType(TileType.Blocker);

				ui.UpdateScore(score, false);
				
				UpdateLevel();
			}
		}

		protected void OnTutorialDone()
		{
			SaveTutorial(false);

			showTutorial = false;

			UnPause();
		}

		protected void LoadHighScore()
		{
			if (PlayerPrefs.HasKey("HighScore"))
			{
				highScore = PlayerPrefs.GetInt("HighScore");
			}
			else
			{
				highScore = 0;
			}
		}

		protected void SaveHighScore(int score)
		{
			PlayerPrefs.SetInt("HighScore", score);
		}

		protected void ResetHighScore()
		{
			SaveHighScore(0);
		}

		protected void LoadTutorial()
		{
			if (PlayerPrefs.HasKey("ShowTutorial"))
				showTutorial = PlayerPrefs.GetInt("ShowTutorial") == 1;
			else
				showTutorial = true;
		}

		protected void ResetTutorial()
		{
			SaveTutorial(true);
		}

		protected void SaveTutorial(bool showTutorial)
		{
			PlayerPrefs.SetInt("ShowTutorial", showTutorial ? 1 : 0);
		}

		#endregion
	}
}


