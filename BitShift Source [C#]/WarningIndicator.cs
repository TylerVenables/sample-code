using UnityEngine;
using System.Collections;

namespace BitShift
{
	public class WarningIndicator : MonoBehaviour
	{
		#region Variables

		public float minAlpha = 0.2f;
		public float maxAlpha = 0.5f;
		public float fadeTime = 0.6f;

		protected int row = 0;

		protected float currentAlpha = 1.0f;
		protected float fadeTimer = 0.0f;
		protected bool fadeOut = false;

		protected Grid grid;
		protected SpriteRenderer spriteRenderer;

		#endregion
		
		#region Properties

		public int Row { get { return row; } }
		public float CurrentAlpha { get { return currentAlpha; } }
		public float FadeTime { get { return fadeTime; } set { fadeTime = value; } }

		#endregion
		
		#region Initialization
		
		public virtual void Awake ()
		{
			grid = GameObject.Find("Game").transform.Find("Grid").GetComponent<Grid>();
			spriteRenderer = transform.Find("Sprite").GetComponent<SpriteRenderer>();
			
			currentAlpha = minAlpha;
			
			SetAlpha(currentAlpha);
			
			SetRow(row);
		}

		public virtual void Start ()
		{

		}
		
		#endregion
		
		#region Game Loop

		public virtual void Update ()
		{
			float fadeAmount = fadeTimer / fadeTime;

			if (fadeOut)
				fadeAmount = 1.0f - fadeAmount;

			float alpha = (maxAlpha - minAlpha) * fadeAmount;

			SetAlpha(minAlpha + alpha);

			fadeTimer += Time.deltaTime;

			if (fadeTimer > fadeTime)
			{
				fadeTimer = 0.0f;

				fadeOut = !fadeOut;
			}
		}
		
		#endregion
		
		#region Public Methods

		public void SetRow(int row)
		{
			this.row = row;

			transform.position = new Vector3(transform.position.x, grid.GetPositionForRow(row), -0.75f);
		}

		#endregion
		
		#region Private Methods

		protected void SetAlpha(float alpha)
		{
			Color c = spriteRenderer.color;
			c.a = alpha;
			spriteRenderer.color = c;

			currentAlpha = alpha;
		}

		#endregion
	}
}
