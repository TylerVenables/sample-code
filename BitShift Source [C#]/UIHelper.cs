using UnityEngine;
using System.Collections;

namespace BitShift
{
	public class UIHelper : MonoBehaviour
	{
		#region Variables

		public GUISkin buttonSkin = null;
		public Vector2 workingResolution = new Vector2(1280, 720);

		private float uiScale = 1.0f;

		private static UIHelper instance;
		
		#endregion
		
		#region Properties

		/*

		280, 150
		3.6
		220, 120
		2
		220, 120
		2.6

		*/

		public static UIHelper Instance
		{
			get
			{
				if (instance == null)
					instance = GameObject.FindObjectOfType<UIHelper>();

				return instance;
			}
		}

		public float UIScale { get { return uiScale; } }

		#endregion
		
		#region Initialization
		
		public virtual void Awake ()
		{
			CalculateUIScale();
		}
		
		public virtual void Start ()
		{

		}
		
		#endregion
		
		#region Game Loop

		public virtual void Update ()
		{
			if (Debug.isDebugBuild)
			{
				CalculateUIScale();
			}
		}
		
		#endregion
		
		#region Public Methods

		public bool Button(string text, Vector2 positionPercent, Vector2 dimensions, float textScale, bool useUIScale = true, bool reposition = false)
		{
			GUISkin prevSkin = GUI.skin;
			GUI.skin = buttonSkin;

			float scaleMult = useUIScale ? uiScale : 1.0f;

			GUI.skin.button.fontSize = (int)(20.0f * textScale * scaleMult);


			Vector2 pos = new Vector2((positionPercent.x * Screen.width) - (dimensions.x * 0.5f * scaleMult),
			                          (positionPercent.y * Screen.height) - (dimensions.y * 0.5f * scaleMult));

			if (reposition)
			{
				pos += GetRepositionedPos(positionPercent);
			}

			bool pressed = GUI.Button(new Rect(pos.x, pos.y, dimensions.x * scaleMult, dimensions.y * scaleMult), text);

			GUI.skin = prevSkin;

			return pressed;
		}

		public void RepositionChildUIElements(Transform parent)
		{
			Transform element = null;
			
			for (int i = 0; i < parent.childCount; i++)
			{
				element = parent.GetChild(i);
				
				if (element.GetComponent<GUIText>() == null && element.GetComponent<GUITexture>() == null)
					continue;

				Vector2 pos = GetRepositionedPos(new Vector2(element.position.x, element.position.y));
				
				if (element.GetComponent<GUIText>() != null)
				{
					((GUIText)element.GetComponent<GUIText>()).pixelOffset = new Vector2(pos.x, pos.y);
				}
				else
				{
					Rect inset = ((GUITexture)element.GetComponent<GUITexture>()).pixelInset;
					inset.x += pos.x;
					inset.y += pos.y;
					
					((GUITexture)element.GetComponent<GUITexture>()).pixelInset = inset;
				}
			}
		}

		public Vector2 GetRepositionedPos(Vector2 percentPos)
		{
			float xDist = (percentPos.x * Screen.width) - (Screen.width * 0.5f);
			float yDist = (percentPos.y * Screen.height) - (Screen.height * 0.5f);
			
			float xTargetDist = (percentPos.x * UIHelper.Instance.workingResolution.x) - (UIHelper.Instance.workingResolution.x * 0.5f);
			float yTargetDist = (percentPos.y * UIHelper.Instance.workingResolution.y) - (UIHelper.Instance.workingResolution.y * 0.5f);

			return new Vector2(xTargetDist - xDist, yTargetDist - yDist);
		}

		#endregion
		
		#region Private Methods

		protected void CalculateUIScale()
		{
			uiScale = Screen.width / workingResolution.x;
			
			float diff = 1.0f - UIScale;
			uiScale = uiScale + (diff * 0.5f);
		}

		#endregion
	}
}



