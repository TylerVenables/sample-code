﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace BitShift
{
	public class Grid : MonoBehaviour
	{
		#region Variables

		public int columns = 10;
		public int rows = 8;
		public float tileSize = 0.7f;

		protected float gridScale = 1.0f;
		protected float adjustedTileSize = 0.7f;
		protected float gridHeight = 0.0f;
		protected float gridWidth = 0.0f;

		protected int halfColumns = 5;
		protected float halfTileSize = 0.35f;

		protected SpriteRenderer sprite = null;

		protected Tile[,] tileGrid;

		#endregion
		
		#region Properties

		public float HalfColumns { get { return halfColumns; } }
		public float HalfTileSize { get { return halfTileSize; } }
		public float GridScale { get { return gridScale; } }
		public float AdjustedTileSize { get { return adjustedTileSize; } }

		#endregion
		
		#region Initialization
		
		public virtual void Awake ()
		{
			sprite = transform.Find("Sprite").GetComponent<SpriteRenderer>();

			CalculateGridScale();

			float gridSize = gridScale * sprite.transform.localScale.x;
			
			sprite.transform.localScale = new Vector3(gridSize, gridSize, 1.0f);
			
			adjustedTileSize = tileSize * gridScale;
			
			gridHeight = rows * adjustedTileSize;
			gridWidth = columns * adjustedTileSize;
			
			halfColumns = Mathf.FloorToInt(columns * 0.5f);
			halfTileSize = adjustedTileSize * 0.5f;

			tileGrid = new Tile[rows, columns];
			ClearGrid();
		}

		public virtual void Start ()
		{


		}
		
		#endregion
		
		#region Game Loop

		public virtual void Update ()
		{

		}
		
		#endregion
		
		#region Public Methods

		public float GetPositionForRow(int row)
		{
			if (row < -0|| row >= rows)
			{
				Debug.LogWarning("Row (" + row + ") out of bounds");
				return 0.0f;
			}

			float pos = ((rows * 0.5f) - row) * adjustedTileSize;
			pos = -(pos - halfTileSize);

			return pos;
		}

		public float GetPositionForColumn(int column)
		{
			if (column < 0 || column >= columns)
			{
				Debug.LogWarning("Column (" + column + ") out of bounds");
				return 0.0f;
			}
			
			float pos = (halfColumns - column) * adjustedTileSize;
			pos = -(pos - halfTileSize);

			return pos;
		}

		public int GetRowForPosition(float position)
		{
			int row = Mathf.FloorToInt((position / adjustedTileSize) + (rows * 0.5f));

			if (row >= rows || row < 0)
				return -1;

			return row;
		}

		public int GetColumnForPosition(float position)
		{
			int col = Mathf.FloorToInt((position / adjustedTileSize) + halfColumns);
			
			if (col >= columns || col < 0)
				return -1;
			
			return col;
		}

		public bool CheckMoveTile(Tile tile, int column, int row)
		{
			bool canMove = true;

			if (row < 0 || row >= rows || column < -1 || column >= columns)
			{
				canMove = false;
			}
			else if (column != -1 && tileGrid[row, column] != null)
			{
				canMove = false;
			}

			return canMove;
		}

		public void MoveTile(Tile tile, int column, int row)
		{
			if (tile.CurrentRow >= 0 && tile.CurrentRow < rows && tile.CurrentColumn >= 0 && tile.CurrentColumn < columns)
				tileGrid[tile.CurrentRow, tile.CurrentColumn] = null;
			
			if (row >= 0 && row < rows && column >= 0 && column < columns)
				tileGrid[row, column] = tile;
		}

		public Tile CheckGrid(int column, int row)
		{
			if (column != -1 && row != -1 && column < columns && row < rows)
			{
				return tileGrid[row, column];
			}

			Debug.LogWarning("Invalid tile coordinates x:" + column + "  y:" + row);
			return null;
		}

		public void ClearGrid()
		{
			for (int col = 0; col < columns; col++)
			{
				for (int row = 0; row < rows; row++)
				{
					tileGrid[row, col] = null;
				}
			}
		}

		public int FindEndOfRow(int row, MoveDirection direction)
		{
			if (row >= 0 && row < rows)
			{
				int currentColumn = halfColumns;

				if (direction == MoveDirection.Right)
					currentColumn--;

				while(currentColumn != -1)
				{
					if (CheckGrid(currentColumn, row) == null)
					{
						break;
					}
					else
					{
						if (direction == MoveDirection.Left)
							currentColumn++;
						else
							currentColumn--;

						if (currentColumn < 0 || currentColumn >= columns)
						{
							currentColumn = -1;
							break;
						}
					}
				}

				return currentColumn;
			}

			return -1;
		}

		public bool CheckForMatches(int minTiles, out List<Tile> matchingTiles)
		{
			bool hasMatches = false;

			matchingTiles = new List<Tile>();

			Tile tile;
			TileColor currentColor = TileColor.Blue;
			List<Tile> currentGroup = new List<Tile>();
			bool changeGroup = false;

			for (int x = 0; x < columns; x++)
			{
				for (int y = 0; y < rows; y++)
				{
					tile = tileGrid[y, x];

					if (tile != null)
					{
						if (currentGroup.Count == 0)
						{
							currentColor = tile.TileColor;
							currentGroup.Add(tile);
						}
						else
						{
							if (tile.TileColor == currentColor)
							{
								currentGroup.Add(tile);
							}
							else
							{
								changeGroup = true;
								currentColor = tile.TileColor;
							}
						}
					}
					else if (currentGroup.Count > 0)
					{
						changeGroup = true;
					}

					if (changeGroup)
					{
						if (currentGroup.Count >= minTiles && currentGroup[0].Type == TileType.Regular)
						{
							matchingTiles.AddRange(currentGroup);
							hasMatches = true;
						}

						currentGroup.Clear();
						changeGroup = false;

						if (tile != null)
						{
							currentColor = tile.TileColor;
							currentGroup.Add(tile);
						}
					}
				}

				if (currentGroup.Count >= minTiles && currentGroup[0].Type == TileType.Regular)
				{
					matchingTiles.AddRange(currentGroup);
					hasMatches = true;
				}

				currentGroup.Clear();
				changeGroup = false;
			}

			return hasMatches;
		}

		public void RemoveTile(Tile tile)
		{
			if (tile.IsInGrid)
			{
				tileGrid[tile.CurrentRow, tile.CurrentColumn] = null;
			}
		}

		public void CollapseTiles(out List<Tile> collapsedTiles)
		{
			int column = 0;
			int checkColumn = 0;
			int row = 0;
			Tile tile = null;

			collapsedTiles = new List<Tile>();

			for (int i = 0; i < halfColumns - 1; i++)
			{
				for (int j = 0; j < rows; j++)
				{
					row = j;

					column = halfColumns + 1 + i;
					tile = tileGrid[row, column];

					if (tile != null && tile.Type != TileType.Bomb)
					{
						for (int k = 0; k < column - halfColumns; k++)
						{
							checkColumn = halfColumns + k;

							if (tileGrid[row, checkColumn] == null)
							{
								MoveTile(tile, checkColumn, row);
								tile.SetCollapsedPosition(checkColumn, row);
								collapsedTiles.Add(tile);
								break;
							}
						}
					}
				
					column = halfColumns - 2 - i;
					tile = tileGrid[row, column];
					
					if (tile != null && tile.Type != TileType.Bomb)
					{
						for (int k = 0; k < halfColumns - column - 1; k++)
						{
							checkColumn = halfColumns - 1 - k;

							if (tileGrid[row, checkColumn] == null)
							{
								MoveTile(tile, checkColumn, row);
								tile.SetCollapsedPosition(checkColumn, row);
								collapsedTiles.Add(tile);
								break;
							}
						}
					}
				}
			}
		}

		public bool CheckForAllTileTypes(int numColors)
		{
			Tile tile;
			int[] tileColorCount = new int[numColors];
			bool hasBlocker = false;

			for (int x = 0; x < columns; x++)
			{
				for (int y = 0; y < rows; y++)
				{
					tile = tileGrid[y, x];
					
					if (tile != null)
					{
						if (tile.Type == TileType.Blocker)
						{
							hasBlocker = true;
						}
						else if (tile.Type == TileType.Regular)
						{
							tileColorCount[(int)tile.TileColor]++;
						}
					}
				}
			}

			bool hasAllTypes = hasBlocker;

			if (hasAllTypes)
			{
				for (int i = 0; i < numColors; i++)
				{
					if (tileColorCount[i] == 0)
					{
						hasAllTypes = false;
						break;
					}
				}
			}

			return hasAllTypes;
		}

		public string GetGridString()
		{
			string gridString = "";
			Tile tile = null;
			bool firstInColumn = true;

			for (int x = 0; x < columns; x++)
			{
				for (int y = 0; y < rows; y++)
				{
					tile = CheckGrid(x, y);

					if (tile != null)
					{
						if (firstInColumn)
						{
							if (gridString.Length > 0)
								gridString += "|";

							gridString += x.ToString();
						}

						gridString += y.ToString() + ((int)tile.TileColor).ToString();

						firstInColumn = false;
					}
				}

				firstInColumn = true;
			}

			return gridString;
		}

		public int CountTilesOfType(TileType type, TileColor color = TileColor.None)
		{
			Tile tile = null;
			int count = 0;

			for (int x = 0; x < columns; x++)
			{
				for (int y = 0; y < rows; y++)
				{
					tile = CheckGrid(x, y);

					if (tile != null && tile.Type == type && (color == TileColor.None || tile.TileColor == color))
						count++;
				}
			}

			return count;
		}

		#endregion
		
		#region Private Methods

		protected void CalculateGridScale()
		{
			float workingRatio = UIHelper.Instance.workingResolution.x / UIHelper.Instance.workingResolution.y;
			float screenRatio = (float)Screen.width / (float)Screen.height;
			
			//Debug.Log("x: " + workingResolution.x + "   y: " + workingResolution.y  + "    " + workingRatio);
			//Debug.Log("x: " + Screen.width + "   y: " + Screen.height  + "    " + screenRatio);
			
			if (screenRatio > workingRatio)
			{
				gridScale = 1.0f;
			}
			else
			{
				float diff = screenRatio / workingRatio;
				
				gridScale = diff;
			}
			
			//Debug.Log(gridScale);
		}

		#endregion
	}
}

