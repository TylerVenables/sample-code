using UnityEngine;
using System.Collections;

namespace BitShift
{
	public class TutorialScreen : MonoBehaviour
	{
		#region Variables

		public Transform[] pages;

		protected int currentPage = 0;
		protected int totalPages = 0;

		protected GUITexture backgroundOverlay;

		public event TutorialDoneHandler TutorialDoneEvent;
		public delegate void TutorialDoneHandler();

		#endregion
		
		#region Properties
		
		#endregion
		
		#region Initialization
		
		public virtual void Awake ()
		{
			backgroundOverlay = transform.Find("BackgroundOverlay").GetComponent<GUITexture>();
		}

		public virtual void Start ()
		{
			backgroundOverlay.pixelInset = new Rect(Screen.width * -0.5f, Screen.height * -0.5f, Screen.width, Screen.height);

			totalPages = pages.Length;

			UIHelper.Instance.RepositionChildUIElements(this.transform);

			for (int i = 0; i < totalPages; i++)
			{
				UIHelper.Instance.RepositionChildUIElements(pages[i].transform);
				pages[i].gameObject.SetActive(false);
			}

			ChangePage(currentPage);
		}
		
		#endregion

		
		#region Game Loop

		public virtual void Update ()
		{
			if (Input.GetMouseButtonDown(0))
			{
				if (currentPage + 1 < totalPages)
				{
					ChangePage(currentPage + 1);
				}
				else
				{
					TutorialDoneEvent();
				}
			}
		}
		
		#endregion
		
		#region Public Methods

		public void ChangePage(int newPage)
		{
			if (newPage < totalPages && newPage >= 0)
			{
				pages[currentPage].gameObject.SetActive(false);
				pages[newPage].gameObject.SetActive(true);

				currentPage = newPage;
			}
		}

		#endregion
		
		#region Private Methods

		#endregion
	}
}


