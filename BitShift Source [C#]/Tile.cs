using UnityEngine;
using System.Collections;

namespace BitShift
{
	public enum TileColor { Blue, Green, Red, Yellow, Purple, Teal, None, Max }
	public enum TileType { Regular, Bomb, Blocker, Max }
	public enum MoveDirection { Left, Right, Max }
	public enum TileState { Idle, Moving, Shifting, Collapsing, LossAnim1, LossAnim2 }

	public class Tile : MonoBehaviour
	{
		#region Variables

		public GameObject explosionEmitter;
		public Sprite bombSpriteAsset;
		public Sprite blockerSpriteAsset;

		protected float moveSpeed = 1.0f;
		protected float shiftSpeed = 2.0f;
		protected float startCollapseSpeed = 1.0f;
		protected float collapseAccel = 15.0f;
		protected float currentSpeed = 0.0f;

		protected float lossAnimTimer = 0.0f;
		protected float lossAnim1Delay = 0.4f;
		protected float minLossAnim2Delay = 0.4f;
		protected float maxLossAnim2Delay = 0.8f;
		protected float lossAnim2Accel = 8.0f;

		protected TileColor color = TileColor.None;
		protected TileType type = TileType.Regular;
		protected MoveDirection moveDirection = MoveDirection.Left;
		protected TileState state;

		protected bool isPaused = false;

		protected Grid grid;
		protected GameSettings gameSettings;
		protected SpriteRenderer sprite;
	
		protected int currentColumn;
		protected int currentRow;
		protected bool isInGrid = false;

		protected float moveDistLeft = 0.0f;

		protected int dirMult;

		public event StateChangedHandler StateChangedEvent;
		public delegate void StateChangedHandler(Tile sender, TileState newState, TileState previousState);

		#endregion

		#region Properties

		public float MoveSpeed { get { return moveSpeed; } set { moveSpeed = value; } }
		public float ShiftSpeed { get { return shiftSpeed; } set { shiftSpeed = value; } }
		public TileColor TileColor { get { return color; } }
		public TileType Type { get { return type; } }
		public MoveDirection MoveDirection { get { return moveDirection; } }
		public TileState State { get { return state; } }
		public int CurrentColumn { get { return currentColumn; } }
		public int CurrentRow { get { return currentRow; } }
		public bool IsInGrid { get { return isInGrid; } }

		#endregion
		
		#region Initialization
		
		public virtual void Awake ()
		{
			grid = GameObject.Find("Game").transform.Find("Grid").GetComponent<Grid>();
			gameSettings = GameObject.Find("Game").transform.Find("GameSettings").GetComponent<GameSettings>();
			sprite = transform.Find("TileSprite").GetComponent<SpriteRenderer>();
			
			currentColumn = -1;
			currentRow = -1;
			
			dirMult = -1;
			
			state = TileState.Moving;
		}

		public virtual void Start ()
		{
			sprite.transform.localScale = new Vector3(grid.GridScale, grid.GridScale, 1.0f);
		}

		public void OnRemove()
		{
			GameObject explosion = (GameObject)GameObject.Instantiate(explosionEmitter, transform.position, Quaternion.identity);
			ParticleSystem emitter = explosion.GetComponent<ParticleSystem>();

			Color explosionColor = gameSettings.tileColors[(int)color];

			if (type == TileType.Blocker)
			{
				explosionColor = new Color(0.5f, 0.5f, 0.5f, 1.0f);
			}
			else if (type == TileType.Bomb)
			{
				emitter.gravityModifier = 0.7f;
				emitter.startLifetime = 0.6f;
				emitter.Emit(30);
			}

			emitter.renderer.material.SetColor("_Color", explosionColor);

			Destroy(explosion, 1.0f);
		}

		#endregion
		
		#region Game Loop

		public virtual void Update ()
		{
			if (!isPaused)
			{
				if (state == TileState.Moving)
				{
					float speed = moveSpeed * Time.deltaTime;

					if (moveDirection == MoveDirection.Left)
						speed = -speed;

					transform.position += new Vector3(speed, 0.0f, 0.0f);

					int nextColumn = grid.GetColumnForPosition(transform.position.x + (grid.HalfTileSize * dirMult));
					int row = grid.GetRowForPosition(transform.position.y);

					if (nextColumn != currentColumn)
					{
						bool stopMoving = false;

						if ((moveDirection == MoveDirection.Left && nextColumn == grid.HalfColumns - 1)  ||
						    (moveDirection == MoveDirection.Right && nextColumn == grid.HalfColumns) ||
						    grid.CheckGrid(nextColumn, row) != null)
						{
							stopMoving = true;
						}

						if (stopMoving)
						{
							SetState(TileState.Idle);
						}
						else
						{
							Move(nextColumn, row);
						}
					}
				}
				else if (state == TileState.Shifting || state == TileState.Collapsing)
				{
					float moveDist = currentSpeed * Time.deltaTime;
					float finalShiftDist = moveDist;

					if (moveDistLeft - moveDist <= 0)
					{
						finalShiftDist = moveDistLeft;
						moveDistLeft = 0.0f;

						SetState(TileState.Idle);
					}
					else 
					{
						moveDistLeft -= moveDist;
					}

					if (moveDirection == MoveDirection.Left)
						finalShiftDist = -finalShiftDist;

					transform.position += new Vector3(finalShiftDist, 0.0f, 0.0f);

					if (state == TileState.Collapsing)
					{
						currentSpeed += collapseAccel * Time.deltaTime;
					}
				}
				else if (state == TileState.LossAnim1 || state == TileState.LossAnim2)
				{
					if (lossAnimTimer <= 0.0f)
					{
						if (state == TileState.LossAnim1)
						{
							SetDisplayColor(sprite.color * 0.6f);

							lossAnimTimer = Random.Range(minLossAnim2Delay, maxLossAnim2Delay);
							SetState(TileState.LossAnim2);
							currentSpeed = 0.0f;
						}
						else
						{
							transform.position += new Vector3(0.0f, -currentSpeed * Time.deltaTime, 0.0f);

							if (transform.position.y < -5.0f)
							{
								currentSpeed = 0.0f;
								SetState(TileState.Idle);
							}

							currentSpeed += lossAnim2Accel * Time.deltaTime;
						}
					}
					else
					{
						lossAnimTimer -= Time.deltaTime;
					}
				}
			}

			if (Debug.isDebugBuild)
			{
				sprite.transform.localScale = new Vector3(grid.GridScale, grid.GridScale, 1.0f);
			}
		}

		public virtual void OnGUI()
		{
			//GUI.Label(new Rect(transform.position.x * 100.0f + 637.0f, 920.0f - (transform.position.y * 100.0f + 570.0f), 50.0f, 50.0f), currentColumn.ToString());
		}

		public void Pause()
		{
			isPaused = true;
		}

		public void UnPause()
		{
			isPaused = false;
		}
		
		#endregion
		
		#region Public Methods

		public void SetDirection(MoveDirection direction)
		{
			moveDirection = direction;

			if (moveDirection == MoveDirection.Left)
				dirMult = -1;
			else
				dirMult = 1;
		}

		public void SetType(TileType newType)
		{
			type = newType;

			if (type == TileType.Bomb)
			{
				sprite.sprite = bombSpriteAsset;
			}
			else if (type == TileType.Blocker)
			{
				sprite.sprite = blockerSpriteAsset;
			}
		}

		public void SetColor(TileColor newColor)
		{
			if (type != TileType.Regular)
				return;

			color = newColor;

			SetDisplayColor(gameSettings.tileColors[(int)color]);
		}

		public void SetDisplayColor(Color color)
		{
			sprite.color = color;
		}

		public void RandomColor(int numColors = (int)TileColor.Max)
		{
			int c = Random.Range(0, numColors);

			SetColor((TileColor)c);
		}

		public void SetRow(int row)
		{
			transform.position = new Vector3(transform.position.x, grid.GetPositionForRow(row), transform.position.z);
			currentRow = row;
		}

		public bool IsInMidColumn()
		{
			return (currentColumn == grid.HalfColumns || currentColumn == grid.HalfColumns - 1);
		}

		public bool Move(int column, int row, bool updatePosition = false)
		{
			int col = column != -1 ? column : currentColumn;

			if (grid.CheckMoveTile(this, col, row))
			{
				if (isInGrid)
					grid.MoveTile(this, col, row);

				currentColumn = col;
				currentRow = row;

				if (updatePosition)
				{
					float x = column == -1 ? transform.position.x : grid.GetPositionForColumn(column);
					float y = row == -1 ? transform.position.y : grid.GetPositionForRow(row);

					//Debug.Log(column + "   " + x);

					transform.position = new Vector3(x, y, transform.position.z);
				}

				return true;
			}

			return false;
		}

		public void SetState(TileState newState)
		{
			if (newState != state)
			{
				TileState prevState = state;
				state = newState;

				if (newState == TileState.Idle && (prevState == TileState.Moving || (prevState == TileState.Shifting && !isInGrid)))
				{
					if (currentColumn != -1)
						isInGrid = true;

					Move(currentColumn, currentRow);
				}
				else if (state == TileState.Shifting)
				{
					if (currentColumn != -1)
					{
						float destX = grid.GetPositionForColumn(currentColumn);
						moveDistLeft = Mathf.Abs(transform.position.x - destX);
					}
					else
					{
						moveDistLeft = grid.AdjustedTileSize;
					}

					currentSpeed = shiftSpeed;
				}
				else if (state == TileState.Collapsing)
				{
					currentSpeed = Random.Range(0.0f, startCollapseSpeed);
				}

				StateChangedEvent(this, state, prevState);
			}
		}

		public void RemoveFromGrid()
		{
			grid.RemoveTile(this);

			currentColumn = -1;
		}

		public void SetCollapsedPosition(int column, int row)
		{
			moveDistLeft = Mathf.Abs(grid.GetPositionForColumn(currentColumn) - grid.GetPositionForColumn(column));

			currentColumn = column;
			currentRow = row;
		}

		public void StartLoseAnimation(bool startLeft)
		{
			SetState(TileState.LossAnim1);

			lossAnimTimer = 0.0f;

			if (currentColumn != -1)
			{
				float colPosition = (float)currentColumn / ((float)grid.columns - 1.0f);

				if (!startLeft)
					colPosition = 1.0f - colPosition;

				lossAnimTimer = colPosition * lossAnim1Delay;
			}
		}

		#endregion
		
		#region Private Methods

		#endregion
	}
}

