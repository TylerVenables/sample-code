This is the spotlight shader that was created for Miner Damage, my final project at Algonquin College. 
The shader features normal mapping, specular lighting, and real time shadows.