struct Light
{
    float3 color;
    float3 direction;
    float3 position;
    float radius;
    float specularPower;
    float intensity;
    float coneAngle;
	float decay;
};

Light light;
float4x4 World;
float4x4 View;
float4x4 Projection;
float3 CameraPosition;
float4x4 InvertViewProjection;
float4x4 LightViewProjection;
texture ColorMap;
texture NormalMap;
texture DepthMap;
texture ShadowMap;
texture EmissiveMap;
float ShadowMapSize;
bool HasShadow;
float2 HalfPixel;

const float ACCURACY_NUM = 0.01;

sampler colorSampler = sampler_state
{
	Texture = (ColorMap);
	AddressU = CLAMP;
	AddressV = CLAMP;
	MagFilter = LINEAR;
	MinFilter = LINEAR;
	MipFilter = LINEAR;
};

sampler normalSampler = sampler_state
{
	Texture = (NormalMap);
	AddressU = CLAMP;
	AddressV = CLAMP;
	MagFilter = POINT;
	MinFilter = POINT;
	MipFilter = POINT;
};

sampler depthSampler = sampler_state
{
	Texture = (DepthMap);
	AddressU = CLAMP;
	AddressV = CLAMP;
	MagFilter = POINT;
	MinFilter = POINT;
	MipFilter = POINT;
};

sampler shadowSampler = sampler_state
{
	Texture = (ShadowMap);
	AddressU = CLAMP;
	AddressV = CLAMP;
	MagFilter = POINT;
	MinFilter = POINT;
	MipFilter = POINT;
};

sampler emissiveSampler = sampler_state
{
	Texture = (EmissiveMap);
	AddressU = CLAMP;
	AddressV = CLAMP;
	MagFilter = POINT;
	MinFilter = POINT;
	MipFilter = POINT;
};

struct VertexShaderInput
{
    float4 Position : POSITION0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
	float4 ScreenPosition : TEXCOORD0;
};

float calculateShadow(float4 texCoords)
{
	float shadowMapDX = 1.0f / ShadowMapSize;
	texCoords /= texCoords.w;

	float2 lightSamplePos;
	lightSamplePos.x = texCoords.x / 2.0f + 0.5f;
	lightSamplePos.y = (-texCoords.y / 2.0f + 0.5f);

	float depth = texCoords.z;
	float mapDepth = tex2D(shadowSampler, lightSamplePos);

	float s1 = tex2D(shadowSampler, lightSamplePos).r;
	float s2 = tex2D(shadowSampler, lightSamplePos + float2(-shadowMapDX, 0)).r;
	float s3 = tex2D(shadowSampler, lightSamplePos + float2(0, -shadowMapDX)).r;
	float s4 = tex2D(shadowSampler, lightSamplePos + float2(-shadowMapDX, -shadowMapDX)).r;

	float r1 = depth <= s1 + ACCURACY_NUM;
	float r2 = depth <= s2 + ACCURACY_NUM;
	float r3 = depth <= s3 + ACCURACY_NUM;
	float r4 = depth <= s4 + ACCURACY_NUM;

	return ((r1 + r2 + r3 + r4) / 4.0f);
}

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;

    float4 worldPosition = mul(input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
	output.ScreenPosition = output.Position;

    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
    input.ScreenPosition.xy /= input.ScreenPosition.w;
	float2 texCoord = 0.5f * (float2(input.ScreenPosition.x, -input.ScreenPosition.y) + 1);

	texCoord -= HalfPixel;

	float4 normalData = tex2D(normalSampler, texCoord);
	float3 normal = 2.0f * normalData.xyz - 1.0f;
	float specularPower = normalData.a * 255;
	float specularIntensity = tex2D(emissiveSampler, texCoord).g;
	float depth = tex2D(depthSampler, texCoord).r;

	float4 position;
	position.xy = input.ScreenPosition.xy;
	position.z = depth;
	position.w = 1.0f;

	position = mul(position, InvertViewProjection);
	position /= position.w;

	float4 lightScreenPos = mul(position, LightViewProjection);
	float shadowFactor = 1;

	if (HasShadow)
		shadowFactor = calculateShadow(lightScreenPos);

	float3 lightVector = light.position - position;

	float d = length(lightVector);

	if (d > light.radius)
		return float4(0.0f, 0.0f, 0.0f, 0.0f);

	float attenuation = saturate(1.0f - d / light.radius);
	lightVector = normalize(lightVector);

	float3 normalizedDir = normalize(light.direction);

	float coneDot = dot(lightVector, normalizedDir);

	if (coneDot > light.coneAngle)
		return float4(0.0f, 0.0f, 0.0f, 0.0f);

	float NdL = max(0, dot(normal, lightVector));
	float3 diffuseLight = NdL * light.color.rgb;

	float3 reflectionVector = normalize(reflect(-lightVector, normal));
	float3 directionToCamera = normalize(CameraPosition - position);
	float specularLight = specularIntensity * pow(saturate(dot(reflectionVector, directionToCamera)), specularPower);
	float decayAmount = pow(coneDot, light.decay);

	return decayAmount * shadowFactor * attenuation * light.intensity * float4((diffuseLight.rgb + tex2D(emissiveSampler, texCoord).r), specularLight);
}

technique Technique1
{
    pass Pass1
    {
        VertexShader = compile vs_3_0 VertexShaderFunction();
        PixelShader = compile ps_3_0 PixelShaderFunction();
    }
}