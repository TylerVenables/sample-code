//
//  AnimationController.m
//  KBo
//
//  Created by Tyler Venables on 11-09-26.
//  Copyright (c) 2011 szello. All rights reserved.
//

#import "AnimationController.h"

@implementation AnimationController

@synthesize frameRate, spriteTexSize, spritesPerRow, isPlaying, loop, loopCount, currentFrame, currentAnimationName, currentAnimation, elapsedAnimTime, animationQueue, playDirection, loopWhenDoneQueue, owner;

- (id)init:(GLuint)f spriteWidth:(GLfloat)w spriteHeight:(GLfloat)h spritesPerRow:(GLuint)r spritesPerColumn:(GLuint)c owner:(NSObject *)o{
    self = [super init];
    if (self){
        animations = [[NSMutableDictionary alloc] init];
        frameRate = f;
        frameTime = 1.0f / frameRate;
        elapsedAnimTime = 0.0f;
        spriteTexSize = GLKVector2Make(w / (w * r), h / (h * c));
        spritesPerRow = r;
        isPlaying = false;
        loop = false;
        loopCount = 0;
        currentFrame = 0;
        previousFrame = 0; 
        currentAnimationName = @"";
		animationQueue = [[NSMutableArray alloc] init];
        playDirection = FORWARD;
		loopWhenDoneQueue = false;
		owner = o;
    }
    
    return self;
}

-(void)setFrameRate:(GLuint)r{
	frameRate = r;
	frameTime = 1.0f / frameRate;
}

-(GLuint)frameRate { return frameRate; }

- (GLKVector2)update:(GLfloat)dt{
    if (isPlaying && [animations count] > 0)
    {
        elapsedAnimTime += dt;

		bool endReached = false;
        
        previousFrame = currentFrame;
		
		if (currentAnimation.length > 1){
        currentFrame = (int)floor(elapsedAnimTime / frameTime) % currentAnimation.length;

        if (playDirection == FORWARD)
            currentFrame = currentAnimation.startFrame + currentFrame;
        else
            currentFrame = currentAnimation.startFrame + currentAnimation.length - currentFrame - 1;
        }
			
		if ((playDirection == FORWARD && previousFrame > currentFrame) || (playDirection == BACKWARD && previousFrame < currentFrame) || (currentAnimation.length == 1 && elapsedAnimTime > frameTime))
			endReached = true;
		
        if (endReached){
            if (loop){
                loopCount--;
                if (loopCount == 0)
                    loop = false;
                else if (loopCount == -1)
                    loopCount = 0;
            }else{
                // FIXIT: EXEC_BAD_ACCESS Crashes here sometimes
				[[NSNotificationCenter defaultCenter] postNotificationName:@"finishedAnimation" object:owner];

				if ([animationQueue count] >= 1){
					[self playAnimation:[animationQueue objectAtIndex:0] deleteQueue:false];
					[animationQueue removeObjectAtIndex:0];
				}else if ([animationQueue count] == 0){
					if (!loopWhenDoneQueue){
						currentFrame = currentAnimation.startFrame + currentAnimation.length - 1;
						isPlaying = false;
						elapsedAnimTime = 0.0f;
					}else{
						currentFrame = currentAnimation.startFrame;
						elapsedAnimTime = 0.0f;
						previousFrame = 0;
					}
				}
            }
        }
    }
    
    return GLKVector2Make((currentFrame % spritesPerRow) * spriteTexSize.x,
                     floor(currentFrame / spritesPerRow) * -spriteTexSize.y);
}

- (void)addAnimation:(NSString *)n startFrame:(GLuint)s{
    animInfo info;
    info.startFrame = s;
    info.length = 1;
    
    [animations setObject:[NSValue valueWithBytes:&info objCType:@encode(animInfo)] forKey:n];
}

- (void)addAnimation:(NSString *)n startFrame:(GLuint)s length:(GLuint)l{
    animInfo info;
    info.startFrame = s;
    info.length = l;
    
    [animations setObject:[NSValue valueWithBytes:&info objCType:@encode(animInfo)] forKey:n];
}

- (GLKVector2)playAnimation:(NSString *)n{
    if (currentAnimationName != n){
        currentAnimationName = n;
        
        animInfo info;
        [((NSValue *)[animations objectForKey:n]) getValue:&info];
        
        currentAnimation = info;
        currentFrame = currentAnimation.startFrame;
        
        isPlaying = true;
    }
    
    loop = false;
    
	[animationQueue removeAllObjects];
	
    return GLKVector2Make((currentFrame % spritesPerRow) * spriteTexSize.x,
                     floor(currentFrame / spritesPerRow) * -spriteTexSize.y);
}

- (GLKVector2)playAnimation:(NSString *)n loop:(bool)l numLoops:(GLuint)c{
    if (currentAnimationName != n){
        currentAnimationName = n;
        
        animInfo info;
        [((NSValue *)[animations objectForKey:n]) getValue:&info];
        
        currentAnimation = info;
        currentFrame = currentAnimation.startFrame;
        
        isPlaying = true;
        elapsedAnimTime = 0.0f;
    }
    
    loop = l;
    loopCount = c;
	
	[animationQueue removeAllObjects];
	
    return GLKVector2Make((currentFrame % spritesPerRow) * spriteTexSize.x,
                     floor(currentFrame / spritesPerRow) * -spriteTexSize.y);
}

- (GLKVector2)playAnimation:(NSString *)n deleteQueue:(bool)d{
    if (currentAnimationName != n){
        currentAnimationName = n;
        
        animInfo info;
        [((NSValue *)[animations objectForKey:n]) getValue:&info];
        
        currentAnimation = info;
        currentFrame = currentAnimation.startFrame;
        
        isPlaying = true;
    }
    
    loop = false;
	
	if (d)
		[animationQueue removeAllObjects];
    
    return GLKVector2Make((currentFrame % spritesPerRow) * spriteTexSize.x,
						  floor(currentFrame / spritesPerRow) * -spriteTexSize.y);
}

- (GLKVector2)playAnimation:(NSString *)n loop:(bool)l numLoops:(GLuint)c deleteQueue:(bool)d{
    if (currentAnimationName != n){
        currentAnimationName = n;
        
        animInfo info;
        [((NSValue *)[animations objectForKey:n]) getValue:&info];
        
        currentAnimation = info;
        currentFrame = currentAnimation.startFrame;
        
        isPlaying = true;
        elapsedAnimTime = 0.0f;
    }
    
    loop = l;
    loopCount = c;
	
	if (d)
		[animationQueue removeAllObjects];
	
    return GLKVector2Make((currentFrame % spritesPerRow) * spriteTexSize.x,
						  floor(currentFrame / spritesPerRow) * -spriteTexSize.y);
}

- (void)addAnimationToQueue:(NSString *)n{
	[animationQueue addObject:n];
}

- (void)restartAnimation{
    currentFrame = currentAnimation.startFrame;
     elapsedAnimTime = 0.0f;
}

- (void)stopAnimation{
    isPlaying = false;
}

@end
