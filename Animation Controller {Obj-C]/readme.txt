This is the animation controller class I created for the game Air Tag. The class was designed to be easy
to use while also supporting many features, such as setting frame rate, reversing animations, and looping.
I also designed it to be portable by making it a standalone class that just returns texture coordinates of
the current frame so it can be used for any game.