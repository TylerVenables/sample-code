//
//  AnimationController.h
//  KBo
//
//  Created by Tyler Venables on 11-09-26.
//  Copyright (c) 2011 szello. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GLKit/GLKit.h>

#define FORWARD 0
#define BACKWARD 1

struct animInfo{
    GLuint startFrame;
    GLuint length;
};
typedef struct animInfo animInfo;

@interface AnimationController : NSObject{
    NSMutableDictionary *animations;
    GLuint frameRate;
    GLfloat frameTime;
    GLfloat elapsedAnimTime;
    GLKVector2 spriteTexSize;
    GLuint spritesPerRow;
    bool isPlaying;
    bool loop;
    GLuint loopCount;
    GLuint currentFrame;
    GLuint previousFrame;
    NSString *currentAnimationName;
    animInfo currentAnimation;
	NSMutableArray *animationQueue;
    GLuint playDirection;
	bool loopWhenDoneQueue;
	__weak NSObject *owner;
}

@property GLuint frameRate;
@property (readonly)GLfloat elapsedAnimTime;
@property GLKVector2 spriteTexSize;
@property GLuint spritesPerRow;
@property (readonly)bool isPlaying;
@property (readonly)bool loop;
@property (readonly)GLuint loopCount;
@property GLuint currentFrame; //(readonly)
@property (strong)NSString *currentAnimationName;
@property animInfo currentAnimation;
@property (readonly, strong)NSMutableArray *animationQueue;
@property GLuint playDirection;
@property bool loopWhenDoneQueue;
@property (readonly) __weak NSObject *owner;

- (id)init:(GLuint)frameRate spriteWidth:(GLfloat)w spriteHeight:(GLfloat)h spritesPerRow:(GLuint)r spritesPerColumn:(GLuint)c owner:(NSObject *)o;
- (GLKVector2)update:(GLfloat)dt;
- (void)addAnimation:(NSString *)n startFrame:(GLuint)s;
- (void)addAnimation:(NSString *)n startFrame:(GLuint)s length:(GLuint)l;
- (GLKVector2)playAnimation:(NSString *)n;
- (GLKVector2)playAnimation:(NSString *)n loop:(bool)l numLoops:(GLuint)c;
- (GLKVector2)playAnimation:(NSString *)n deleteQueue:(bool)d;
- (GLKVector2)playAnimation:(NSString *)n loop:(bool)l numLoops:(GLuint)c deleteQueue:(bool)d;
- (void)addAnimationToQueue:(NSString *)n;
- (void)restartAnimation;
- (void)stopAnimation;

@end
